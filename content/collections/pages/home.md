---
id: home
blueprint: home
title: 'Rent your very own time machine'
template: home
subtitle: '**With the latest technology**, our time machine will get you somewhere.'
teaser: 'Ever gone 88 mph in a Delorean, buddy?'
show_lightning_bolts: true
updated_by: 8d7e674f-4098-45db-8aa3-8a6d2761d70e
updated_at: 1680251142
panels:
  -
    id: lfv6e4k1
    panel_image: istockphoto-1386831709-170667a.jpg
    panel_header: 'This is heavy!'
    panel_content: "If you're going to build a time machine into a car, why not do it with some style?"
    cta_text: 'Pop the Hood'
    cta_link: '#'
    type: new_set
    enabled: true
    panel_teaser: 'This is Heavy'
    panel_heading: 'The power to change history.'
  -
    id: lfva0qga
    panel_image: istockphoto-1397503546-170667a.jpg
    panel_teaser: 'Your kids are gonna love it.'
    panel_heading: 'Control your density'
    panel_content: "Become who you were meant to be one day, today. Or relive yesterday. It's up to you."
    cta_text: 'Book Now'
    cta_link: '#'
    type: new_set
    enabled: true
---
